#include <boost/mpi.hpp>
#include <boost/serialization/string.hpp>
#include <string>
#include <iostream>
#include <unistd.h>
#include <cmath>
#include <chrono>
#include <limits>

// Source Wolfram Alpha
#define EXACT 97.6207830277844692057077374287606726933375763805918378688587426054597163067369825608773128104953403 
#define ACCURACY 1e-5
using namespace std;

typedef double (*func) (double);

double f(double x) {
    return fabs(exp(x)*sin(8*x));
}

double doubleSame(double a, double b) {
    return std::fabs(a - b) < std::numeric_limits<double>::epsilon();
}

double calcSimpson(double lBound, double rBound, func f, double stepSize);
double calcSimpsonMPI(double lbound, double rbound, func f, double stepSize, int rank, int size);

int main(int argc, char** argv) {
    func fu =  f;

    int rank, size;

    boost::mpi::environment env{argc, argv};
    boost::mpi::communicator world;

    double simSumA;
    double simSum;

    bool repeateLoop = false;

    std::chrono::steady_clock::time_point start;
    std::chrono::steady_clock::time_point end;

    double lBound, rBound, stepSize;
    lBound = 0.0;
    rBound = 5.0;
    stepSize = 0.001;
    double offset = (rBound-lBound)/world.size();

    if(world.rank() == 0) {
        cout.precision(16);
        start = chrono::steady_clock::now();
        double sim = calcSimpson(lBound, rBound, f, stepSize);
        end = chrono::steady_clock::now();
        cout << "Simpson: " << sim << endl;
        cout << "Accuracy: " << EXACT - sim << endl;
        auto elapsed = chrono::duration_cast<chrono::milliseconds>(end-start);
        cout << "Runtime: " << elapsed.count() << " ms" << endl;
    }

    if(world.rank() == 0)
        cout << "Running on " << world.size() << " Threads" << endl;



    do {
        start = chrono::steady_clock::now();

        simSumA = calcSimpsonMPI((world.rank()*offset), ((world.rank()+1)*offset)-stepSize, f, stepSize, world.rank(), world.size());

        boost::mpi::all_reduce(world, simSumA, simSum, std::plus<double>());

        simSum /= 3.0;

        end = chrono::steady_clock::now();

        if(world.rank() == 0) {
            cout << world.rank() << "> " << "Reduced Sum: " << simSum << endl;
            cout << "Accuracy: " << EXACT - simSum << endl;
            auto elapsed = chrono::duration_cast<chrono::milliseconds>(end-start);
            cout << "Runtime: " << elapsed.count() << " ms" << endl;
        }
        repeateLoop = !(fabs(EXACT-simSum) <= ACCURACY);
        if(repeateLoop)
            stepSize /= 10;

    } while(repeateLoop);
    return (0);
}

double calcSimpson(double lBound, double rBound, func f, double stepSize) {
    double sum = 0.0;
    bool squared = true;
    for(double n = lBound; n <= rBound; n +=stepSize) {
        if(n==lBound || n>=rBound)
            sum += fabs(f(n)*stepSize);
        else
        {
            sum += ((!squared) ? 2 : 4) * fabs(f(n)*stepSize);
            squared = (!squared) ? true : false;
        }
    }
    sum /= 3.0;
    return(sum);
}
double calcSimpsonMPI(double lbound, double rbound, func f, double stepSize, int rank, int size) {
    double sum = 0.0;
    bool squared = true;
    for(double n = lbound; n <= rbound; n +=stepSize) {
        if((n==lbound && rank == 0) || (doubleSame(n, (rbound+stepSize)) && rank == (size-1))) {
            sum += fabs(f(n)*stepSize);
        }
        else
        {
            sum += ((!squared) ? 2 : 4) * fabs(f(n)*stepSize);
            squared = (!squared) ? true : false;
        }
    }
    return(sum);
}
