#include <iostream>
#include <mpi.h>
#include <unistd.h>
#include <cmath>
#include <chrono>

// Source Wolfram Alpha
#define EXACT 97.6207830277844692057077374287606726933375763805918378688587426054597163067369825608773128104953403 

using namespace std;

typedef double (*func) (double);

double f(double x) {
    return fabs(exp(x)*sin(8*x));
}

double calcSimpson(double lBound, double rBound, func f, double stepSize);
double calcSimpsonMPI(double lbound, double rbound, func f, double stepSize);

int main(int argc, char** argv) {
    func fu =  f;
    int rank, size;
    MPI_Comm mpiComm;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    double simSumA;
    double simSum;
    simSumA = 42;


    double lBound, rBound, stepSize;
    lBound = 0.0;
    rBound = 5.0;
    stepSize = 0.0000001;
    double problemSize = ceil((rBound-lBound)/stepSize);
    unsigned long probsPerRank = problemSize/size;
    double offset = (rBound-lBound)/size;
    //    if(MPI_Barrier(MPI_COMM_WORLD) != MPI_SUCCESS)
    //        cerr << "ERROR: MP_Barrier at " << __LINE__ << endl;

    if(rank == 0) {
        cout.precision(16);
        auto start = chrono::system_clock::now();
        double sim = calcSimpson(lBound, rBound, f, stepSize);
        auto end = chrono::system_clock::now();
        cout << "Simpson: " << sim << endl;
        cout << "Accuracy: " << EXACT - sim << endl;
        auto elapsed = chrono::duration_cast<chrono::microseconds>(end-start);
        cout << "Runtime: " << elapsed.count() << " µs" << endl;
        cout << "Ranks: " << size << endl;
        cout << "Problems: " << problemSize << " Probs/Rank: " << probsPerRank << endl;
    }

    simSumA = calcSimpsonMPI((rank*offset), ((rank+1)*offset)-stepSize, f, stepSize);

    //MPI_Reduce(&simSumA, &simSum, size, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    MPI_Reduce(&simSumA, &simSum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    simSum /= 3.0;

    if(rank == 0) {
        cout << rank << "> " << "Reduced Sum: " << simSum << endl;
        cout << "Accuracy: " << EXACT - simSum << endl;
    }
    MPI_Finalize();
    return (0);
}

double calcSimpson(double lBound, double rBound, func f, double stepSize) {
    double sum = 0.0;
    bool squared = true;
    for(double n = lBound; n <= rBound; n +=stepSize) {
        if(n==lBound || n>=rBound)
            sum += fabs(f(n)*stepSize);
        else
        {
            sum += ((!squared) ? 2 : 4) * fabs(f(n)*stepSize);
            squared = (!squared) ? true : false;
        }
    }
    sum /= 3.0;
    return(sum);
}
double calcSimpsonMPI(double lbound, double rbound, func f, double stepSize) {
    double sum = 0.0;
    bool squared = true;
    for(double n = lbound; n <= rbound; n +=stepSize) {
        if(n==lbound || n>=(rbound+stepSize)) {
            sum += fabs(f(n)*stepSize);
        }
        else
        {
            sum += ((!squared) ? 2 : 4) * fabs(f(n)*stepSize);
            squared = (!squared) ? true : false;
        }
    }
    //sum /= 3.0;
    return(sum);
}
