#include "TMeasure.h"

int main(int argc, char** argv)
{
	TMeasure tm;

	auto begin = std::chrono::high_resolution_clock::now();

	tm.mark("foo");
	usleep(5000000);
	tm.mark("bar");
	usleep(5000000);
	tm.mark("bärchen");

	std::cout << tm.report() << std::endl;

	std::string filename("test.txt");

	if(tm.foo(filename))
		std::cout << "Datei geschrieben" << std::endl;
	else
		std::cout << "Datei nicht geschrieben" << std::endl;


	auto ende = std::chrono::high_resolution_clock::now();

	std::cout << (std::chrono::duration_cast<std::chrono::microseconds>(ende - begin).count()) << " usec" << std::endl;
}
