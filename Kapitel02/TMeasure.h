#ifndef TMEASURE_H
#define TMEASURE_H
#include <iostream>
#include <cstdlib>
#include <sys/time.h>
#include <sys/resource.h>
#include <string>
#include <chrono>
#include <vector>
#include <fstream>
#include <sstream>
#include <unistd.h>

struct messpunkt
{
	std::string _mark;
	struct rusage _rusage;
	std::string _hrc;
};

typedef struct messpunkt messpunkt;

class TMeasure 
{
	public:
	TMeasure()
   	{
	   	std::cout << "TM init ...\n";
		start = std::chrono::high_resolution_clock::now();
   	}
	~TMeasure() 
	{ 
		std::cout << "TM destroy ...\n";
		markPoints.clear();
   	}

	void mark(std::string markPoint);
	std::string report();
	bool foo(std::string filename);

	private:
	std::vector<messpunkt> markPoints;
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
	//std::chrono::time_point<std::chrono::high_resolution_clock> end;
};

#endif
