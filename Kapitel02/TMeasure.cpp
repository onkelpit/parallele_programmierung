#include "TMeasure.h"

void TMeasure::mark(std::string markPoint)
{
	std::cout << "mark stub" << std::endl;
	messpunkt tm;

	tm._mark = markPoint;
	getrusage(0, &tm._rusage);

	auto end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << std::endl;
	tm._hrc = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();

	markPoints.push_back(tm);
}

std::string TMeasure::report()
{
	std::string newLine = "\n";
	std::stringstream ret;
	std::string tab = "\t";
	ret.str("");
	for(auto tm : markPoints)
	{
		ret << tm._mark + newLine;
		ret << "ru_utime.tv_use" << tab << tm._rusage.ru_utime.tv_usec << newLine;
		ret << "high_resolution" << tab << tm._hrc << newLine;
		ret <<newLine;
	}
	ret << "stub";
	return(ret.str());
}

bool TMeasure::foo(std::string filename)
{
	if(!filename.empty())
	{
		std::ofstream file;
		file.open(filename);
		file << report();
		file.close();
		return true;
	}

	else
		return false;
}
