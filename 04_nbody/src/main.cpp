#include <iostream>
#include <iomanip>
#include <ostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <string>
#include <omp.h>

#include "util.h"

using namespace std;

#define GAMMA 6.67408E-11
// -14 da NASA Daten in km und nicht m
/* #define GAMMA 6.67408E-14 */
/* http://www.cosmiq.de/qa/show/3593453/Wie-lautet-die-Gravitationskonstante-in-astronomischen-Einheiten/ */
/* #define GAMMA 1.99372E-44 */

#define AU 149597870.700e+3

#define MINUTE      60
#define HOUR        3600
#define DAY         86400
#define YEAR        31536000

#ifdef _DEBUG_
#define myEndl " in LINE: " << __LINE__ << endl;
#else
#define myEndl endl;
#endif


void calcForces();
void calcPositions(double dt);
void calcSpeeds(double dt);

struct P {
    double x, y, z;
    P()
        : x(0)
        , y(0)
        , z(0){};
    P(double const& param)
        : x(param)
        , y(param)
        , z(param)
    {
    }
    P(double _x, double _y, double _z)
        : x(_x)
        , y(_y)
        , z(_z){};
    P(P const& p)
        : x(p.x)
        , y(p.y)
        , z(p.z){};

    P operator+(P const& p)
    {
        P t = P(*this);
        t.x += p.x;
        t.y += p.y;
        t.z += p.z;
        return t;
    }
    P operator+(double const& d)
    {
        P t = P(*this);
        t.x += d;
        t.y += d;
        t.z += d;
        return t;
    }

    P operator*=(P const &p) {
        x *= p.x;
        y *= p.y;
        z *= p.z;
        return *this;
    }

    P operator*=(double const& d) {
        x *= d;
        y *= d;
        z *= d;
        return *this;
    }

    P operator+=(P const &p) {
        x += p.x;
        y += p.y;
        z += p.z;
        return *this;
    }

    P operator+=(double const& d) {
        x += d;
        y += d;
        z += d;
        return *this;
    }

    P operator=(P const &p) {
        x = p.x;
        y = p.y;
        z = p.z;
        return *this;
    }

    P operator-(P const& p)
    {
        P t = P(*this);
        t.x -= p.x;
        t.y -= p.y;
        t.z -= p.z;
        return t;
    }
    /* P operator-(double const& d) */
    /* { */
    /*     *this - P(d); */
    /*     return *this; */
    /* } */

    P operator*(P const& p)
    {
        P t = P(*this);
        t.x *= p.x;
        t.y *= p.y;
        t.z *= p.z;
        return t;
    }
    P operator*(double const& d)
    {
        P t = P(*this);
        t.x *= d;
        t.y *= d;
        t.z *= d;
        return t;
    }

    P operator/(P const& p)
    {
        P t = P(*this);
        if (p.x != 0 && p.y != 0 && p.z != 0) {
            t.x /= p.x;
            t.y /= p.y;
            t.z /= p.z;
        }
        return t;
    }

    P operator/(double const& d)
    {
        P t = P(*this);
        if (d != 0)
        {
            t.x /= d;
            t.y /= d;
            t.z /= d;
        }
        return t;
    }

    bool operator==(P const& other) const
    {
        if (x == other.x && y == other.y && z == other.z)
            return true;
        return false;
    }

    bool operator!=(P const& other) const { return !(*this == other); }

    double n()
    {
        double len = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        return len;
    }

    P norm()
    {
        double len = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        x *= 1 / len;
        y *= 1 / len;
        z *= 1 / len;
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, P const& p) { 
        return os << p.x << " " << p.y << " " << p.z ;
    }

    friend P operator*(double lhs, P rhs) {
        P foo(rhs);
        foo.x *= lhs;
        foo.y *= lhs;
        foo.z *= lhs;
        return foo;
    }
};

struct Planet {
    P s;
    P v;
    double m;
    P f;
    string name;

    Planet(P _s, P _v, double _m, P _f, string _name)
        : s(_s)
        , v(_v)
        , m(_m)
        , f(_f)
        , name(_name)
    {
    }

    bool operator==(Planet const& other) const
    {
        if (s == other.s && v == other.v && m == other.m && f == other.f) {
            return true;
        }
        return false;
    }

    bool operator!=(Planet const& other) const {
       if (s == other.s && v == other.v && m == other.m && f == other.f) {
            return false;
        }
        return true;
    }

    friend std::ostream& operator<<(std::ostream& os, Planet const& pl) { 
        return os << "Name: " << pl.name << std::endl << "Position: " << pl.s 
            << std::endl << "Velocity: " << pl.v << std::endl << "Mass: " 
            << pl.m << std::endl << "Force: " << pl.f; 
    }

    void write() {
        ofstream file;
        file.open("output/" + name + ".txt", fstream::app);
        file << setprecision(16) << s << endl;
        file.close();
    }

    void positionExpPlus3() {
        s.x *= 1E3;
        s.y *= 1E3;
        s.z *= 1E3;
    }

    void normVelocityVectors() {
        v *= (AU/DAY);
    }

    void normToSun(P &vs, P &vv) {
        s.x -= vs.x;
        s.y -= vs.y;
        s.z -= vs.z;

        v.x -= vv.x;
        v.y -= vv.y;
        v.z -= vv.z;
    }

    void normStationVector() {
        s *= AU;
    }
};

vector<Planet> planets;

int main()
{

    /*
     * Start time      : A.D. 2015-Dec-16 00:00:00.0000 CT
     * m in kg
     */

    planets.push_back(Planet(
        P(5.518015788199366E+08,  1.639051615970429E+08, -2.389699662753655E+07),
        P(2.287934078612447E-00,  1.164651885433457E1, -6.917412448953844E-02),
        1.9884e30, 
        P(), 
        "Sonne"
    ));
    planets.push_back(Planet(
        P(8.731557005807579E+10,  1.199814062403729E+11, -2.898766001993418E+07),
        P(-2.462045087777594E+04,  1.736636875655618E+04, -4.279176783494876E-02),  
        5.97e24, 
        P(), 
        "Erde"
    ));
    planets.push_back(Planet(
        P(-2.241126017574772E+11,  1.077735490954381E+11,  7.744884948104680E+09),
        P(-9.555518287702174E+03, -1.977269816975537E+04, -1.800643674120739E2), 
        0.642e24, 
        P(), 
        "Mars"
    ));
    planets.push_back(Planet(
        P(-5.877548684469981E+11, -1.375241589623157E+12,  4.730373474565303E+10), 
        P(8.353834167002478E+03, -3.826162197834356E+03, -2.653675745022648E2), 
        568e24, 
        P(), 
        "Saturn"
    ));


    /* planets.push_back(Planet( */
    /*     P(1.711651664398659E-01, -8.884091156115568E-01, -3.851400501774775E-01) ,  /// Sone (AU) */
    /*     P(1.721626633528917E-02,  2.811539463256241E-03,  1.218216822296422E-03) ,  /// Sone (AU) */
    /*     1.98892E30, */ 
    /*     P(0), */ 
    /*     "Sun") */
    /* ); */
    /* planets.push_back(Planet( */
    /*     P(7.208224854447207E-01, -1.309983009016502E+00, -6.095994550816987E-01) ,  /// Venus */
    /*     P(3.032906388475589E-02,  1.698670261512789E-02,  6.766361744946199E-03) ,  /// Venus */
    /*     4.867E24, */
    /*     P(0), */
    /*     "Venus" */
    /* )); */
    /* planets.push_back(Planet( */
    /*     P(0.000000000000000E+00,  0.000000000000000E+00,  0.000000000000000E+00) ,  /// Erde */
    /*     P(0.000000000000000E+00,  0.000000000000000E+00,  0.000000000000000E+00) ,  /// Erde */
    /*     5.972E24, */ 
    /*     P(0), */ 
    /*     "Earth") */
    /* ); */
    /* planets.push_back(Planet( */
    /*     P(1.527014935495353E+00, -1.125152714001175E+00, -5.303319566123560E-01) ,  /// Mars */
    /*     P(2.053509135026218E-02,  1.640039962343657E-02,  7.361458618340400E-03) ,  /// Mars */
    /*     6.39E23, */
    /*     P(0), */
    /*     "Mars" */
    /* )); */
    /* planets.push_back(Planet( */
    /*     P(-3.558402622173626E+00,  2.565694903278902E+00,  1.186183607163296E+00),   /// Jupiter */
    /*     P(1.173966320092903E-02, -1.775589449943693E-03, -6.145988032223847E-04) ,  /// Jupiter */
    /*     1898.13E24, */
    /*     P(0), */
    /*     "Jupiter" */
    /* )); */
    /* planets.push_back(Planet( */
    /*     P(-5.840484369624553E+08, -1.531138296341320E+09,  4.665794019541359E+07), */
    /*     P(3.877868279000385E+01, -6.935271144885217E+00, -2.851133270061630E-01), */
    /*     568.51E24, */
    /*     P(0), */
    /*     "Saturn" */
    /* )); */
    /* planets.push_back(Planet( */
    /*     P(2.809068340156094E+09,  8.256453597593631E+08, -3.297915692192739E+07), */
    /*     P(2.810437448219503E+01,  2.875665712602299E+00,  3.699733120585869E-02), */
    /*     86.849E24, */
    /*     P(0), */
    /*     "Uranus" */
    /* )); */
    /* planets.push_back(Planet( */
    /*     P(4.161991634757305E+09, -1.765607571349324E+09, -6.294654862232900E+07), */
    /*     P(3.229711386058295E+01,  1.854266447024311E+00, -1.646829490915509E-01), */
    /*     102.44E24, */
    /*     P(0), */
    /*     "Neptun" */
    /* )); */


    /* for (auto &p : planets) { */
    /*     p.normStationVector(); */
    /*     p.normVelocityVectors(); */
    /*     if(!p.name.compare("Sun")) */
    /*         p.normToSun(planets.at(0).s, planets.at(0).v); */
    /* } */

    planets.at(0).s = P(0);
    planets.at(0).v = P(0);

    cout.precision(4);

    /* for(size_t i = 1; i <= 365; i++) { */
    /*     for (auto& p : planets) { */
    /*         calcForceVector(p, planets); */
    /*         calcPositions(p, planets, DAY) ; */
    /*         calcVelocityVector(p, DAY); */
    /*         p.write(); */
    /*     } */
    /* } */

    for(int i = 0; i<700*24;i++){  
      calcForces();
      calcPositions(HOUR);
      calcSpeeds(HOUR);
      /* printPos(planets.at(1)); */
      for(Planet &p : planets)
          p.write();
    }
        return 0;
}

void calcForces()
{
   #pragma omp parallel for
  for(int iP = 0; iP < planets.size(); iP++){
    P tForces = P();
    Planet tPlanet = planets.at(iP);
    for(int jP = 0; jP < planets.size(); jP++){
      if(tPlanet != planets.at(jP)){
	P diffPos = ((planets.at(0).s - planets.at(iP).s) - (planets.at(0).s - planets.at(jP).s));
	tForces += (diffPos / (diffPos.n() * diffPos.n() * diffPos.n())) * planets.at(jP).m;
      }
    }
    tForces *= GAMMA;
    tForces *= tPlanet.m;
    planets.at(iP).f = P(tForces);
  }
}

void calcPositions(double dt)
{
#pragma omp parallel for
  for(int iP = 0; iP < planets.size();iP++){
    P tPos;
    Planet tPlanet = planets.at(iP);
    tPos = tPlanet.s + (tPlanet.v * dt) + (tPlanet.f / tPlanet.m)*dt*dt*0.5;
    planets.at(iP).s = tPos;
  }
}

void calcSpeeds(double dt)
{
#pragma omp parallel for
  for(int iP = 0; iP < planets.size(); iP++){
    P tSpeed;
    Planet tPlanet = planets.at(iP);
    tSpeed = tPlanet.v + (tPlanet.f / tPlanet.m)*dt;
    planets.at(iP).v = tSpeed;
  }
}

