/*
 * =====================================================================================
 *
 *       Filename:  test.cpp
 *
 *    Description:  test for util.h
 *
 *        Version:  1.0
 *        Created:  28.10.2015 16:28:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Peter Johennecken (), peter.johennecken(a)gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */
#include <iostream>
#include <unistd.h>
#include <thread>
#include "util.h"

void foo() {
    int i;
}

int main(int argc, char** argv) {
    TM tm1("foo");


    TM tm2("bar", false);
    sleep(5);
    tm2.start();
    std::cout << tm1.stop() << std::endl;
    sleep(5);
    std::cout << tm2.stop() << std::endl;
    TM tm3("thread");
    std::thread t1(foo);
    std::cout << tm3.stop() << std::endl;
    t1.join();
    return 0;
}
