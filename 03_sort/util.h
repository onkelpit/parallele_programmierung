/*
 * =====================================================================================
 *
 *       Filename:  util.h
 *
 *    Description:  Utils for MPI
 *
 *        Version:  1.0
 *        Created:  28.10.2015 16:06:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Peter Johennecken (), peter.johennecken(a)gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <string>
#include <sstream>
#include <chrono>

#ifndef _UTIL_H_
#define _UTIL_H_

using namespace std;
struct TM {
    private:
        chrono::steady_clock::time_point begin;
        string mark = "";

    public:
        TM(string Mark) {
            mark = Mark;
            begin = chrono::steady_clock::now();
        }

        TM(string Mark, bool active) {
            if(active) {
                mark = Mark;
                begin = chrono::steady_clock::now();
            }
            else
                mark = Mark;
        }

        void start() {
            begin = chrono::steady_clock::now();
        }

        string stop() {
            chrono::steady_clock::time_point end = chrono::steady_clock::now();
            stringstream ret;
            ret.str(mark);
            ret << mark << "> Duration: " << chrono::duration_cast<chrono::seconds>(end-begin).count() << " sec " <<
                chrono::duration_cast<chrono::milliseconds>(end-begin).count() << " ms " <<
                chrono::duration_cast<chrono::microseconds>(end-begin).count() << " us " <<
                chrono::duration_cast<chrono::nanoseconds>(end-begin).count() << " ns";
            return ret.str();
        }
};

#endif
