/*
 * =====================================================================================
 *
 *       Filename:  sort.cpp
 *
 *    Description:  MPI application to test stdSort against QS + MS in parallel
 *
 *        Version:  1.0
 *        Created:  28.10.2015 16:06:21
 *       Revision:  none
 *       Compiler:  g++
 *
 *         Author:  Peter Johennecken (), peter.johennecken(a)gmail.com
 *   Organization:
 *
 * =====================================================================================
 */
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdexcept>
#include <sstream>

#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/operations.hpp>
#include <boost/serialization/vector.hpp>

#include "util.h"

#ifdef _DEBUG_
#define myEndl " in LINE: " << __LINE__ << endl;
#else
#define myEndl endl;
#endif

namespace mpi = boost::mpi;

using namespace std;

void stdSort(vector<int>* A);
void MS(mpi::communicator* world, vector<int>* A);
void randNumVector(vector<int>* V, int* len);
bool vertify(vector<int>& A);

int main(int argc, char** argv)
{

    int len = 0;
    vector<int>* V = new vector<int>();
    mpi::environment env;
    mpi::communicator world;

    int offset;

    if (world.rank() == 0) {
        if (argc > 1) {
            istringstream ss(argv[1]);
            if (!(ss >> len)) {
                cerr << "Parameterangabe (" << argv[1] << ") keine Ganzzahl" << myEndl;
                world.abort(2);
            }
        }
        randNumVector(V, &len);
        cout << "Zufallsvektor: ";
        for (auto& v : *V)
            cout << v << " ";
        cout << myEndl;
    }

    mpi::broadcast(world, len, 0);

    offset = V->size() / world.size();

    world.barrier();
    if(world.rank() == 0) {
        TM tm1("stdSort");
        tm1.start();
        sort(V->begin(), V->end());
        cout << tm1.stop() << myEndl;
    }

    if (len % world.size() != 0 && world.rank() == 0) {
        cerr << len << " % " << world.size() << " = " << len % world.size() << myEndl;
        cerr << "Objekte muessen ein vielfaches von der Anzahl der "
             << "Threads sein" << myEndl;
        world.abort(1);
        return (0);
    }

    if (world.rank() == 0) {
        for (int i = 1; i < world.size(); i++) {
            world.send(i, i, new vector<int>(V->begin() + i * offset, V->begin() + (i * offset) + offset));
        }
        V = new vector<int>(V->begin(), V->begin() + offset);
    }
    else {
        world.recv(0, world.rank(), V);
    }
    world.barrier();

    sort(V->begin(), V->end());
    cout << world.rank() << "> Vektor: ";
    for (auto& v : *V)
        cout << v << " ";
    cout << myEndl;

    TM tm2("MergeSort");
    tm2.start();
    MS(&world, V);
    world.barrier();
    vector<vector<int> > res;
    mpi::all_gather(world, *V, res);

    if(world.rank() == 0)
        cout << tm2.stop() << myEndl;

    vector<int> result;
    for (auto& r : res) {
        for (auto& R : r)
            result.push_back(R);
    }

    if (world.rank() == 0) {
        cout << world.rank() << "> RESULT: ";
        for (auto& res : result)
            cout << res << " ";
        cout << myEndl;

        /* cout << "Vektor sortiert: " << boolalpha << vertify(result) << myEndl; */
    }

    delete V;
    return (0);
}

void stdSort(vector<int>* A)
{
    TM tm1("stdSort");
    tm1.start();
    sort(A->begin(), A->end());
    cout << tm1.stop() << myEndl;
}

void MS(mpi::communicator* world, vector<int>* A)
{
    int partner = -1;

    bool even = world->rank() % 2 == 0;
    bool even_round = false;
    bool ready = false;
    bool partnerRight = false;

    vector<int>* B = new vector<int>();
    vector<mpi::request> req;
    int round_counter = 0;

    do {
        round_counter++;
        ready = false;
        /**
         * errechnen des Partner Prozesses
         * even && !even_round => rechter Partner
         * even && even_round => linker Partner
         * !even && !even_round => linker Partner
         * !even && even_round => rechter Partner
         *      => wenn rechter Partner muss Kernel auf <= prüfen vice versa
         */
        if (even) {
            partner = world->rank() + ((even_round) ? -1 : +1);
        }
        else {
            partner = world->rank() + ((even_round) ? +1 : -1);
        }

        if (partner < 0 || partner >= world->size())
            partner = -1;

        if (world->rank() + 1 == partner)
            partnerRight = true;
        else
            partnerRight = false;

#ifdef _DEBUG_
        cout << world->rank() << "> Partner: " << partner << " Partner right: " << boolalpha << partnerRight << " in Round " << round_counter << myEndl;
#endif

        if (partner != -1) {
            /**
             * Versenden und Empfangen der Puffer
             */
            req.push_back(world->isend(partner, partner, A));
            req.push_back(world->irecv(mpi::any_source, world->rank(), B));
            mpi::wait_all(req.begin(), req.begin() + req.size());

            vector<int>* C = new vector<int>(*A);
            unsigned int index = 0;
            unsigned int indexP = 0;

            A->clear();

            if (partnerRight) {
                do {
                    if (indexP >= C->size()) {
                        A->push_back(C->at(index));
                        index++;
                    }
                    else if (C->at(index) <= B->at(indexP)) {
                        A->push_back(C->at(index));
                        index++;
                    }
                    else if (B->at(indexP) <= C->at(index)) {
                        A->push_back(B->at(indexP));
                        indexP++;
                    }
                    else
                        indexP++;
                } while (A->size() < C->size());
            }
            else {
                index = C->size() - 1;
                indexP = B->size() - 1;
                do {
                    if (indexP >= C->size()) {
                        A->push_back(C->at(index));
                        index--;
                    }
                    else if (C->at(index) > B->at(indexP)) {
                        A->push_back(C->at(index));
                        index--;
                    }
                    else if (B->at(indexP) > C->at(index)) {
                        A->push_back(B->at(indexP));
                        indexP--;
                    }
                    else
                        indexP--;
                } while (A->size() < C->size());
                vector<int> foo(*A);
                reverse_copy(foo.begin(), foo.end(), A->begin());
            }
        }

        // es werden world->size()/2 Durchläufe benötigt
        // danach ready auf true setzen
        if (round_counter >= world->size())
            ready = true;
        else
            ready = false;

        req.clear();
        world->barrier();
        even_round = !even_round;
        mpi::all_reduce(*world, ready, ready, mpi::bitwise_and<bool>());
    } while (!ready);
}

void randNumVector(vector<int>* V, int* len)
{
    if (*len == 0)
        *len = 32;
    srand(time(0));
    for (int i = 0; i < *len; i++)
        V->push_back(rand() % 100);
}

bool vertify(vector<int>& A)
{
    auto it = A.begin();

    for (; it != A.end(); ++it) {
        auto foo = std::next(it);
        if (*it > *foo) {
            cerr << *it << " less or eq than " << *foo << myEndl;
            cerr << "not sorted" << endl;
            return false;
        }
    }
    return true;
}
